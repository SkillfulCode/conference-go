<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/WayneBasile/conference-go/-/blob/main/README.md?ref_type=heads">
    <img src="ghi/app/public/readme-logo.svg" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Conference Go</h3>

  <p align="center">
    Welcome to Conference Go! Whether you're an event organizer, speaker, attendee, or sponsor, our application is here to make your conference participation seamless and efficient. Let's work together to make your upcoming events a resounding success!
    <br />
    <br />
    <a href="https://gitlab.com/WayneBasile/conference-go/-/blob/main/README.md?ref_type=heads"><strong>Explore the Docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/WayneBasile/conference-go/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/WayneBasile/conference-go/-/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#local-installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

<p>
  <img src="Homepage.png" width="49%" />
  <img src="Conferences.png" width="49%" />
</p>

Conference Go is a conference management service built with agile software development practices and Domain Driven Design patterns that transform a simple, monolithic Django application into a stand-alone web application. The project seamlessly integrates messaging middleware and microservice architecture to create a robust, fault-tolerant, and scaleable web application.

### Built With

[![Django][Django.com]][Django-url] &nbsp; [![React][React.js]][React-url] &nbsp; [![RabbitMQ][RabbitMQ.com]][RabbitMQ-url]

[![Docker][Docker.com]][Docker-url] &nbsp; [![Bootstrap][Bootstrap.com]][Bootstrap-url] &nbsp; [![HTML5][HTML5.com]][HTML5-url]

[![Python][Python.org]][Python-url] &nbsp; [![Javascript][Javascript.com]][Javascript-url] &nbsp; [![Insomnia][Insomnia.rest]][Insomnia-url]

[![ReactRouter][ReactRouter.com]][ReactRouter-url] &nbsp; [![PostgreSQL][PostgreSQL.org]][PostgreSQL-url] &nbsp; [![CSS3][CSS3.com]][CSS3-url]

<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

The installation instructions assume your system has the following software: [Google Chrome](https://www.google.com/chrome/) and [Docker](https://www.docker.com/).

If you don't have these (or equivalent) software, please install them before proceeding.

To get a local copy of Health and Swolleness up and running on your machine follow these simple steps.

### Local Installation

1. Fork and clone the [repository](https://gitlab.com/WayneBasile/conference-go/-/forks/new)

2. In terminal, run `cd <repository_directory>`

3. Rename the .env.sample file to .env

4. Add a [Pexels API Key](https://www.pexels.com/api/new/)

5. Add an [Open Weather API Key](https://home.openweathermap.org/api_keys)

6. In terminal, run `docker compose build`

7. In terminal, run `docker compose up`

8. Navigate to [localhost:3000](http://localhost:3000/)



<!-- CONTACT -->
## Contact

[![Contributors][wayne-shield]][wayne-url]



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

[Django](https://www.djangoproject.com/) · [React](https://react.dev/) · [React Router](https://reactrouter.com/en/main) · [Insomnia](https://insomnia.rest/)

[Docker](https://www.docker.com/) · [Bootstrap](https://getbootstrap.com/) · [HTML5](https://developer.mozilla.org/en-US/docs/Web/HTML) · [RabbitMQ](https://www.rabbitmq.com/)

[Python](https://www.python.org/) · [Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) · [PostgreSQL](https://www.postgresql.org/)

[Shields.io](https://shields.io/) · [Simple Icons](https://simpleicons.org/) · [Pexels](https://www.pexels.com/) · [CSS3](https://developer.mozilla.org/en-US/docs/Web/CSS)

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[Django.com]: https://img.shields.io/badge/Django-092E20?style=for-the-badge&logo=Django&logoColor=white
[Django-url]: https://www.djangoproject.com/

[React.js]: https://img.shields.io/badge/React-61DAFB?style=for-the-badge&logo=react&logoColor=white
[React-url]: https://reactjs.org/

[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-7952B3?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com

[Docker.com]: https://img.shields.io/badge/Docker-2496ED?style=for-the-badge&logo=docker&logoColor=white
[Docker-url]: https://www.docker.com/

[HTML5.com]: https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white
[HTML5-url]: https://developer.mozilla.org/en-US/docs/Web/HTML

[Python.org]: https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white
[Python-url]: https://www.python.org/

[Javascript.com]: https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=white
[Javascript-url]: https://developer.mozilla.org/en-US/docs/Web/JavaScript

[PostgreSQL.org]: https://img.shields.io/badge/PostgreSQL-4169E1?style=for-the-badge&logo=postgresql&logoColor=white
[PostgreSQL-url]: https://www.postgresql.org/

[ReactRouter.com]: https://img.shields.io/badge/React_Router-CA4245?style=for-the-badge&logo=reactrouter&logoColor=white
[ReactRouter-url]: https://reactrouter.com/en/main

[wayne-shield]: https://img.shields.io/badge/Wayne_Basile-0A66C2?logo=linkedin&style=for-the-badge
[wayne-url]: https://www.linkedin.com/in/waynebasile/

[RabbitMQ.com]: https://img.shields.io/badge/RabbitMQ-FF6600?style=for-the-badge&logo=rabbitmq&logoColor=white
[RabbitMQ-url]: https://www.rabbitmq.com/

[Insomnia.rest]: https://img.shields.io/badge/Insomnia-4000BF?style=for-the-badge&logo=insomnia&logoColor=white
[Insomnia-url]: https://insomnia.rest/

[CSS3.com]: https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white
[CSS3-url]: https://developer.mozilla.org/en-US/docs/Web/CSS
